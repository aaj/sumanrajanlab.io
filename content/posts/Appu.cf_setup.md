---
title: "Appu.cf setup Part(1)"
date: 2019-08-23T14:55:08+05:30
draft: false
toc: false
images:
tags: [GNU/Linux,website]
---

This posts attempts to break down technologies that have been used for this site ```appu.cf``` , hoping it will pave way and inspire others
 to get started in their online presence

Appu.cf is setup with zero investments and no hidden charges included , yeah you heard it right everything was setuped for free and this is a
poc (Proof of concept) that such a thing is do able so lets dive in .

Technologies used :-

* Gitlab pages & Git
* Hugo
* Cloudflare
* Freenom 

#### Gitlabpages
Its a feature from gitlab which allows one to run a static website from git repository, onething to note its totally free for gitlab users
To publish a website with Pages, you can use any Static Site Generator (SSG), such as Jekyll, Hugo, Middleman, Harp, Hexo, and Brunch, just to
name a few. You can also publish any website written directly in plain HTML, CSS, and JavaScript.
Pages does not support dynamic server-side processing, for instance, as .php and .asp requires.

Git is what we use for version controlling and guessing you are familier with git bascis such as add commit & push .

#### [Hugo](https://gohugo.io/) 
There are many ssg's(Static Site Generator) out there but we will be using hugo here because Hugo is the world’s fastest static website engine
 and it’s written in Go (aka Golang), working with hugo makes is fun since one doenst need to worry about underlying html,css&jss . 

#### [Cloudflare](https://www.cloudflare.com/en-in/)
You might have come across 1.1.1.1 dns service , yeah its from them . Cloudflare, Inc is an American web infrastructure and website
 security company, providing content delivery network services, DDoS mitigation, Internet security, and distributed domain name server services.
we will be using cloudflare dns services for our website and its literally free for 1 year with ssl certificates.

#### [Freenom](https://freenom.com)
Freenom is from where we get our free domain for 1 year , afak it  is the only TLD (Top Level Domain) which is free for a limited period .

## Installation 
I am having a [debian](https://www.debian.org) 10(Sid) machine ,let me shoutout something in between **Debian is awesome** and if you are a windows guy you are own your own (tata) , we will need to install  hugo (extended version)
 and git
 
```
sudo apt update && sudo apt install git -y
```

 get [hugo extended version] (https://github.com/gohugoio/hugo/releases) and install it 

```
debian@localhost:~/Downloads$ sudo apt install ./hugo_extended_0.57.2_Linux-64bit.deb
```
with this we are one stepclose to our website.
 
